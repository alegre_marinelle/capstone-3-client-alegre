import {useContext, Fragment} from 'react';
import {Navbar, Nav} from 'react-bootstrap';

//Import Link component from NextJS
import Link from 'next/link'

import UserContext from '../userContext';


export default function NavBar(){

	const {user} = useContext(UserContext);
	// console.log(user)

	return (

		<Navbar expand="lg" variant="dark">
			<Link href="/">
				<a className="navbar-brand">Thrifty</a>
			</Link>
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav className="ml-auto">
					<Link href="/">
						<a className="nav-link" role="button">Home</a>
					</Link>
					{
						user.email
						?
						<Fragment>
							<Link href='/categories'>
								<a className="nav-link" role="button">Category</a>
							</Link>
							<Link href='/records'>
								<a className="nav-link" role="button">Record</a>
							</Link>
							<Link href='/search'>
								<a className="nav-link" role="button">Search</a>
							</Link>
							<Link href='/comparison'>
								<a className="nav-link" role="button">Budget Analysis</a>
							</Link>
							<Link href='/breakdown'>
								<a className="nav-link" role="button">Category Breakdown</a>
							</Link>
							<Link href='/profile'>
								<a className="nav-link" role="button">Profile</a>
							</Link>
							<Link href='/logout'>
								<a className="nav-link" role="button">Logout</a>
							</Link>
						</Fragment>
						:
						<Fragment>
							<Link href='/login'>
							<a className="nav-link" role="button">Login</a>
							</Link>
							<Link href='/register'>
								<a className="nav-link" role="button">Register</a>
							</Link>
							<Link href='/about'>
								<a className="nav-link" role="button">About</a>
							</Link>
						</Fragment>
					}

				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)

}