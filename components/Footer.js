export default function Footer(){

	return(

		<footer className="footer fixed-bottom mt-2">
			<p className="p-3 text-center"> Thrifty | Marinelle A. Alegre &copy; 2021</p>
		</footer>
	)
}