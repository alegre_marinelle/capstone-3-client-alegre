import {Fragment} from 'react';
import {Card, Col} from 'react-bootstrap';

import moment from 'moment';

export default function Record({recordProp}){

	// console.log(recordProp)

	const {_id, type, category, amount, description, balance, recordedOn} = recordProp
	

	return(
 
			<Fragment>
				<Col md={6}>
					<Card key={_id} className="my-3">
						<Card.Body>
							<Card.Title>Category Name: <strong>{category}</strong></Card.Title>
							<Card.Text>Type: {type}</Card.Text>
							<Card.Text>Amount: &#8369;{amount}</Card.Text>
							<Card.Text>Desc: {description}</Card.Text>
							<Card.Text>Balance: &#8369;{balance}</Card.Text>
							<Card.Text>Recorded On {moment(recordedOn).format("LL")}</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Fragment>
	)

}