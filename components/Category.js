import {Fragment} from 'react';
import {Card, Col, Row} from 'react-bootstrap';

export default function Category({categoryProp}){

	// console.log(categoryProp)
	
	const {_id, name, type} = categoryProp;


	return(

			<Card key={_id} className="my-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>{type}</Card.Text>
				</Card.Body>
			</Card>
	)

}