import {Fragment, useState, useEffect, useContext} from 'react';
import Head from 'next/head';
import Image from 'next/image';
import {Row, Col, Card, Container, Jumbotron, Button, Form} from 'react-bootstrap';

//usercontext
import UserContext from '../../userContext';

export default function About(){
	return (
		<Fragment>
			<Head>
	            <title>About | Thrifty</title>
	            <link rel="icon" href="/wallet.png" />
	        </Head>
	        <h1 className="text-center my-5">About</h1>
			<Container>
				<Row>
					<Col sm={12} md={6} lg={6} className="my-3">				
						<Card className="bg-light mt-1 mx-auto h-100 bg-transparent about ">
						  <Card.Body>
						    <Card.Title className="mb-3">Thrifty</Card.Title>
						    <Card.Subtitle className="mb-4 text-muted">About the Application</Card.Subtitle>
						    <Card.Text>
						      <strong>BUDGETING.</strong> It sometimes seems like a road that is too long, or a staircase that is too high, you can’t always see what the end goal is. In order for you to move forward, is to SIMPLY START. TODAY. It takes faith and courage to start, but I know you can do it. That's all it takes.
						    </Card.Text>
						    <Card.Text>That's when Thrifty comes into the action.</Card.Text>
						    <Card.Text>Thrifty is a Budget Tracking System that enables the user to keep track of their incomes and expenses.</Card.Text>
						  </Card.Body>
						</Card>
					</Col>
					<Col sm={12} md={6} lg={6} className="my-3">				
						<Card className="bg-light mt-1 mx-auto bg-transparent about">
							<Card.Body>
							    <Card.Title>About the Developer</Card.Title>
							    <Card.Subtitle className="mb-4 text-muted">Hello!</Card.Subtitle>
							    <Card.Text>I'm Marinelle, a Full Stack Developer, and I am the Developer of Thrifty!</Card.Text>
							    <Card.Text>Feel free to contact me through my details:</Card.Text>
							    <Card.Text>
							    	<Container className="text-center mb-3">
								    	<Card.Link>
							      			<a href="https://gitlab.com/alegre_marinelle" target="_blank"role="button">Gitlab</a>
							      		</Card.Link>
							      		<Card.Link>
	        								<a href="https://www.linkedin.com/in/marinelle-alegre-9a6285198/" target="_blank" role="button">LinkedIn</a>
							      		</Card.Link>
							    		<h6 className="my-1">Email: marinellaalegre@gmail.com</h6>
							    	</Container>
							    </Card.Text>
							    <Card.Text className="mt-5">
							    	<h5>Credits for Site Logo: </h5>
							    	Icons made by <a href="https://www.freepik.com" title="Freepik" role="button" target="_blank">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon" role="button" target="_blank">www.flaticon.com</a></Card.Text>
							</Card.Body>
					    	<Card.Footer className="text-center bg-transparent about">
								<Button variant="secondary" className="btn-block" href="https://alegre_marinelle.gitlab.io/capstone1-alegre/" target="_blank">Check my profile</Button>
					    	</Card.Footer>
						</Card>
					</Col>
				</Row>
			</Container>
	    </Fragment>

	)
}
