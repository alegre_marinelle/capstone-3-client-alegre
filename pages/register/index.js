import {Fragment, useEffect, useState, useContext} from 'react';
import Head from 'next/head';

import {Form, Button, Card, Row, Col, Container} from 'react-bootstrap';

//import router from nextJS for user redirection
import Router from 'next/router';
import Link from 'next/link';

//import sweet alert
import Swal from 'sweetalert2';

export default function Register(){

	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [password1,setPassword1] = useState("");
	const [password2,setPassword2] = useState("");

	//state for conditionally rendering the submit button
	const [isActive,setIsActive] = useState(true);

	useEffect( () => {
		// console.log("Hello, I'm being run by a useEffect hook.")

		if((firstName !== "" && lastName != "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){

			setIsActive(true)

		} else {

			setIsActive(false)
		}

	}, [firstName, lastName, email, password1, password2])

	function registerUser(e){

		e.preventDefault()

		if(isActive) {
			fetch('https://peaceful-cove-93097.herokuapp.com/api/users/email-exists', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({
					email: email
				})

			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				if (data === false) {

					fetch('https://peaceful-cove-93097.herokuapp.com/api/users/register', {
						method: 'POST',
						headers: { 'Content-Type': 'application/json' },
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1
						})
					})
					.then(res => res.json())
					.then(data => {

						console.log(data);

						if(data === true){
							//clear the input fields / state to initial value
							setFirstName("")
							setLastName("")
							setEmail("")
							setPassword1("")
							setPassword2("")

							Swal.fire({
								icon: "success",
								title: "Successfully Registered",
								text: "Thank you for registering!"
							})

							//redirect to login page
							Router.push('/login');

						} else {

							// error in creating registration
							Swal.fire({
								icon: "error",
								title: "Unsuccessful Registration",
								text: "Something went wrong!"
							})
						}
					})
				} else {
					Swal.fire({
						icon: "error",
						title: "Unsuccessful Registration",
						text: "Email already exists!"
					})
				}
			})
		}	
	} //end of function

	return(
		<Fragment>
			<Head>
	            <title>Register | Thrifty</title>
	            <link rel="icon" href="/userprofile.png" />
	        </Head>
			<Container>
				<Row>
					<Col xs={12} md={6} lg={6} className="offset-md-3 mt-5">
						<Card>
							<Card.Body>
								<Card.Title>Register</Card.Title>
								<Form onSubmit={e => registerUser(e)} autoComplete="off">
									<Form.Group controlId="userFirstName">
											<Form.Label>
												First Name
											</Form.Label>
											<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e)=> setFirstName(e.target.value)} required />
										</Form.Group>
										<Form.Group controlId="userLastName">
											<Form.Label>
												Last Name
											</Form.Label>
											<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e)=> setLastName(e.target.value)} required />
										</Form.Group>
										<Form.Group controlId="userEmail">
											<Form.Label>
												Email
											</Form.Label>
											<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e)=> setEmail(e.target.value)} required />
										</Form.Group>
										<Form.Group controlId="userPassword1">
											<Form.Label>
												Password
											</Form.Label>
											<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={(e)=> setPassword1(e.target.value)} required />
										</Form.Group>
										<Form.Group controlId="userPassword2">
											<Form.Label>
												Confirm Password
											</Form.Label>
											<Form.Control type="password" placeholder="Enter Password" value={password2} onChange={(e)=> setPassword2(e.target.value)} required />
										</Form.Group>
										<Button variant="secondary" type="submit" className="btn-block my-2" disabled={!isActive}>Register</Button>
								</Form>
								<Link href="/login"><a className="ml-auto" role="button">Already have an account? Login</a></Link>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
			
		</Fragment>
	)
}