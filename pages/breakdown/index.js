import {Fragment, useState, useEffect} from 'react';
import Head from 'next/head';

import {Pie, Doughnut} from 'react-chartjs-2';

import moment from 'moment';

import randomcolor from 'randomcolor'

import {Row, Col, Container, Card, Button, Form} from 'react-bootstrap';

import 'chartjs-plugin-labels';

export default function Breakdown(){

	//initial states
	//category breakdown
	const [categories, setCategories] = useState([]);
	const [amount, setAmount] = useState([]);

	//income / expense breakdown
	const [income, setIncome] = useState([]);
	const [expense, setExpense] = useState([]);

	//states to get start and end date
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	//state for all records
	const [allRecords, setAllRecords] = useState([]);

	//state to check user date inputs
	const [isActive, setIsActive] = useState(false);

	//check if user input in date range
	useEffect(() => {

		if(startDate !== '' && endDate !== ''){

			setIsActive(true);

		} else {

			setIsActive(false);
		}

	}, [startDate, endDate])

	// get all records of expense and income
	useEffect(() => {
		fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/records`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			//set value for all records
			setAllRecords(data);

			//for expense
			let expenseCategories = [];
			let amountExpense = [];

			//for income
			let incomeCategories = [];
			let amountIncome = [];

			// //filter expenses
			let categoriesExpense = data.filter(e => e.type === "expense");
			let categoriesIncome = data.filter(e => e.type === "income");

			//map category names using the filtered data for expense
			expenseCategories = categoriesExpense.map(e => e.category);
			incomeCategories = categoriesIncome.map(e => e.category);

			//map amount using the filtered data
			amountExpense = categoriesExpense.map(e => e.amount);
			amountIncome = categoriesIncome.map(e => e.amount);

			//set value for Category Breakdown
			setAmount(amountExpense);
			setCategories(expenseCategories);

			//get income and expense balances
			//expense balance
			let loss = 0;
			let gastos = amountExpense.forEach(amount => {
				// console.log(amount)
				loss = loss + parseInt(amount);

			})
			//set value for total expense
			setExpense(loss);

			let gain = 0;
			let nakuha = amountIncome.forEach(amount => {
				gain = gain + parseInt(amount)
			});
			//set value for total income
			setIncome(gain);
			
		})

	}, [])

	let colors = categories.map(e => randomcolor());

	const data = {
		labels: categories,
		datasets: [{
			backgroundColor: colors,
			data: amount
		}]
	}

	const data2 = {
		labels: ["Income", "Expense"],
		datasets: [{
			backgroundColor: colors,
			data: [income, expense]
		}]
	}

	//options to be added to set
	const options = {

		scales: {

			yAxes: [
				{
					ticks: {
						beginAtZero: true,
						// max: 100
					}

				}
			]
		},
		responsive: true,
		plugins:{
		  	labels: [
			    {
			      render: 'percentage',
			      position: 'outside',
			      arc: true,
			      fontColor: '#000'
			    }
		  	]
		}
	}

	const PieOptions = {

		scales: {

			yAxes: [
				{
					ticks: {
						beginAtZero: true,
						// max: 100
					}

				}
			]
		},
		responsive: true,
		plugins:{
		  	labels: [
			    {
			      render: 'percentage',
			      precision: 2,
			      fontColor: '#000'
			    }
		  	]
		}
	}

	function dateRange(e){
		e.preventDefault();

		/*
			INPUT DATE FORMAT = "YYYY-MM-DD"

			check all records that is within the range of the date,
			if T then get categories and type and balances.
			- get categories array and expense balance array // amount & categories

			- get total income and expense balances //income & expense

		*/

		let result = allRecords.filter(check => {
			if(moment(check.recordedOn).format("YYYY-MM-DD") === startDate || moment(check.recordedOn).format("YYYY-MM-DD") === endDate){
				return check;
			}
		})

		//temp array to get categories of expense within the date range
		let expCategory = [];
		let expBalance =[];

		//temporary placeholders
		let totalInc = 0;
		let totalExp = 0;


		//get all category with expense as type
		result.map(e => {
			if( e.type === "expense"){

				expCategory.push(e.category);
				expBalance.push(e.balance);
				totalExp = totalExp + parseInt(e.balance);

			} else {

				totalInc = totalInc + parseInt(e.balance);

			}
		});
		//set new values (depending of the date range)
		setCategories(expCategory);
		setAmount(expBalance);
		setExpense(totalExp);
		setIncome(totalInc);
	}

	return(
		<Fragment>
			<Head>
	            <title>Category Breakdown | Thrifty</title>
	            <link rel="icon" href="/chart.png" />
	        </Head>
	        <Container className="my-3 mt-4">
	        <h1 className="text-center my-2">Breakdown</h1>
	        	<Row>
		        	<Col>
		        		<Container>
		        			<Card className="my-3">
		        				<Card.Body>
		        					<Card.Title>Date Range</Card.Title>
		        					<Form onSubmit={e => dateRange(e)}>
		        						<Form.Row>
		        							<Form.Group as={Col} controlId="startDate">
			        							<Form.Label>Start Date</Form.Label>
			        							<Form.Control type="date" value={startDate} onChange={e => setStartDate(e.target.value)}/>
			        						</Form.Group>
			        						<Form.Group as={Col} controlId="endtDate">
			        							<Form.Label>End Date</Form.Label>
			        							<Form.Control type="date" value={endDate} onChange={e => setEndDate(e.target.value)}/>
			        						</Form.Group>
		        						</Form.Row>
		        						<Button className="btn-block" type="submit" variant="secondary" disabled={!isActive}>Submit</Button>
		        					</Form>
		        				</Card.Body>
		        			</Card>
			        	</Container>
		        	</Col>
		        </Row>
		        <Row className="my-5">
		        	<Col xs={12} md={6} lg={6} className="mt-2">
	        			<h3 className="text-center">Income/Expense</h3>
		        		<Pie data={data2} options={PieOptions} />
		        	</Col>
		        	<Col xs={12} md={6} lg={6} className="mt-2">
	        			<h3 className="text-center">Category Breakdown</h3>
	        			<Doughnut data={data} options={options} />
		        	</Col>
		        </Row>
	        </Container>
		</Fragment>
	)
}