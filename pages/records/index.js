import {Fragment, useState, useEffect, useContext} from 'react';
import Head from 'next/head';

import {Row, Col, Card, Container, Button, Form, InputGroup} from 'react-bootstrap';
//components
import Record from '../../components/Record'

//usercontext
import UserContext from '../../userContext';

//import router from nextJS for user redirection
import Router from 'next/router';

//import sweet alert
import Swal from 'sweetalert2';

export default function Records(){

	const {user} = useContext(UserContext);

	//states of form
	const [categoryName, setCategoryName] = useState('');
	const [type, setType] = useState('');
	const [amount, setAmount] = useState('');
	const [description, setDescription] = useState('');
	const [recordedOn, setRecordedOn] = useState('');

	//get token after initial render
	const [token, setToken] = useState('');

	//states to get records and balance from db
	const [balance, setBalance] = useState(0);
	const [allRecords, setAllRecords] = useState([]);

	//states to save results
	const [records, setRecords] = useState([]);
	const [incomeRecords, setIncomeRecords] = useState([]);
	const [expenseRecords, setExpenseRecords] = useState([]);
	const [names, setNames] = useState([]); //all category name

	// state for conditionally rendering the submit button
	const [isActive, setIsActive] = useState(false);

	//overview
	const [choice, setChoice] = useState("");

	//check user's input
	useEffect(() => {

		if(categoryName !== '' && type !== '' && amount !== 0 && description !== '' && recordedOn !== '') {

			setIsActive(true)
			setToken(localStorage.getItem('token'))

		} else {

			setIsActive(false)

		}

	}, [categoryName, type, amount, description, recordedOn])

	//get user balance
	useEffect(() => {

		fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/balance`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setBalance(data))

	}, [balance])

	//get all records
	useEffect(() => {

		fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/records`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllRecords(data)
			setIncomeRecords(data.filter(record => record.type === "income"))
			setExpenseRecords(data.filter(record => record.type === "expense"))

		})

	}, [])

	//get user categories
	useEffect(() => {

		fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/categories`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			//temp Arrays
			let inArr = [];
			let exArr = [];
			let arr = []; // blank array for default

			if(type === "income"){
				// console.log(data.filter(cat => cat.type === "income"))
				data.filter(cat => {
					if(cat.type === "income") {
						inArr.push({_id: cat._id, name: cat.name})
					}
				})
				// console.log(inArr)
				setNames(inArr)
			} else if(type === "expense"){
				data.filter(cat => {
					if(cat.type === "expense"){
						exArr.push({_id: cat._id, name: cat.name})
					}
				})
				// console.log(exArr)
				setNames(exArr)
			} else {

				setNames(arr)
			}
		})

	}, [type])

	//FILTER
	useEffect(() => {

		if(choice === "Income"){

			setRecords(incomeRecords)

		} else if(choice === "Expense") {

			setRecords(expenseRecords)

		} else {

			setRecords(allRecords)

		}

	}, [choice, allRecords, incomeRecords, expenseRecords])

	//SHOW RESULTS
	//show category names 
	const categoryForm = names.map(result => {
		return(
			<option key={result._id}>{result.name}</option>
		)
	})

	//cards to show records
	const recordCards = records.map(record => {
		return(
			<Record key={record._id} recordProp={record}/>
		)
	})

	//FUNCTION
	function addRecord(e){

		e.preventDefault()

		// let initialBal = balance;
		let amt = parseInt(amount);
		let newBalance = 0;

		if(type === "income"){

			newBalance = balance + amt

		} else {

			newBalance = balance - amt
		}

		fetch('https://peaceful-cove-93097.herokuapp.com/api/users/addRecord', {
			method: "POST",
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				id: user.userId,
				type: type,
				category: categoryName,
				amount: amount,
				description: description,
				balance: newBalance,
				recordedOn: new Date(recordedOn)
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)
			if(data) {

				fetch('https://peaceful-cove-93097.herokuapp.com/api/users/addBalance', {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						id: user.userId,
						balance: newBalance
					})
				})
				.then(res => res.json())
				.then(data => {
					//console.log(data);
					setBalance(newBalance);
				})

				setCategoryName('');
				setType('');
				setAmount(0);
				setDescription('')

				Swal.fire({
					icon: "success",
					title: "Record Added",
					text: "Successfully added a record."
				})

				Router.push('/records');
				//windows.replace.location('/records')

			} else {

				Swal.fire({
					icon: "error",
					title: "Unsuccessful Creation",
					text: "Record Creation Failed. Something went wrong."
				})
			}
		})

	}

	return (

		<Fragment>
			<Head>
	            <title>Records | Thrifty</title>
	            <link rel="icon" href="/file.png" />
	        </Head>
			<h1 className="text-center my-3">Records Overview</h1>
			<Container>
				<Row>
					<Col xs={12} md={4} lg={4}>
						<Card className="my-3" style={{ width: '20rem' }}>
							<Card.Body>
							    <Card.Title className="mb-4">Balance: &#8369;{balance}</Card.Title>
							    <Card.Title>Add Record</Card.Title>
							    <Form onSubmit={e => addRecord(e)} autoComplete="off">
									<Form.Group controlId="type">
										<Form.Label>Type</Form.Label>
										<Form.Control as="select" value={type} onChange={e => setType(e.target.value)}>
												<option value="" disabled>Select Type</option>
												<option>income</option>
												<option>expense</option>
										    </Form.Control>
									</Form.Group>
									<Form.Group controlId="categoryName">
										<Form.Label>Category Name</Form.Label>
										<Form.Control as="select" value={categoryName} onChange= {e => setCategoryName(e.target.value)}>
											<option value="" disabled>Select Category</option>
											{categoryForm}
										</Form.Control>
									</Form.Group>
									<Form.Group controlId="amount">
										<Form.Label>Amount</Form.Label>
										<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e => setAmount(e.target.value)} required />
									</Form.Group>
									<Form.Group controlId="description">
										<Form.Label>Description</Form.Label>
										<Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required />
									</Form.Group>
									<Form.Group controlId="recordedOn">
										<Form.Label>Date</Form.Label>
										<Form.Control type="date" value={recordedOn} onChange={e => setRecordedOn(e.target.value)} required />
									</Form.Group>
									<Button type='submit' variant="secondary" className="btn-block" disabled={!isActive}>Add Record
									</Button>
								</Form>
							</Card.Body>
						</Card>
					</Col>
					<Col xs={12} md={8} lg={8}>
						<Container className="my-3 text-center cards">
							<Row >
								<InputGroup>
							    <InputGroup.Prepend>
							      <InputGroup.Text>Records</InputGroup.Text>
							    </InputGroup.Prepend>
							    	<Form.Control as="select" value={choice} onChange={e => setChoice(e.target.value)}>
								    	<option>All</option>
								    	<option>Income</option>
								    	<option>Expense</option>
								    </Form.Control>
							  </InputGroup>
							  {recordCards}
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>	
	)
}