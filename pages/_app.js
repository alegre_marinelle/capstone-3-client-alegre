import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css'

import Head from 'next/head';
//import hooks from react
import{Fragment, useState, useEffect} from 'react';

//Components
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

//Bootstrap Components
import {Container} from 'react-bootstrap';

//import our UserProvider
import {UserProvider} from '../userContext'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null,
		userId: null
	})

	// console.log(user)

	useEffect(() => {

		setUser({

			email: localStorage.getItem('email'),
			userId: localStorage.getItem('userId')
		})

	}, [])

	const unsetUser = () => {

		localStorage.clear();

		//set the values of our state back to its initial value

		setUser({

			email: null,
			userId: null
		})

	}


	return (
		<Fragment>
			<Head>
                <link rel="preconnect" href="https://fonts.gstatic.com" />
				<link href="https://fonts.googleapis.com/css2?family=Mitr&family=Pacifico&display=swap" rel="stylesheet" />
            </Head>
			<UserProvider value={{user, setUser, unsetUser}} >
				<NavBar />
				<Container fluid className="mainC">
					<Component {...pageProps} />
				</Container>
				<Footer />
			</UserProvider>
		</Fragment>
	)
}

export default MyApp

/*
<link rel="preconnect" href="https://fonts.gstatic.com" />
                <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+JP&family=Pacifico&display=swap" rel="stylesheet" />

*/