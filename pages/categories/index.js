import {Fragment, useState, useEffect, useContext} from 'react';
import Head from 'next/head';

import {Row, Col, Card, Table, Container, Button, Form} from 'react-bootstrap';

//components
import Category from '../../components/Category'

//import router from nextJS for user redirection
import Router from 'next/router';

//import sweet alert
import Swal from 'sweetalert2';

//usercontext
import UserContext from '../../userContext';

export default function Categories(){

	const {user} = useContext(UserContext);
	// console.log(user)
	let id = user.userId;

	const [categoryName, setCategoryName] = useState('');
	const [type, setType] = useState('');

	const [categories, setCategories] = useState([]);

	// state for conditionally rendering the submit button
	const [isActive, setIsActive] = useState(false);

	//check user's input
	useEffect(() => {

		if(categoryName !== '' && type !== '') {

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	}, [categoryName, type])

	// get all categories
	useEffect(() => {

		fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/categories`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => setCategories(data))

	}, [])

	const categoryCards = categories.map(category => {
		/*
			<Category key={category._id} categoryProp={category} />
		*/
		return (
			<tr key={category._id}>
				<td>{category.name}</td>
				<td>{category.type}</td>
			</tr>
		)
	})


	function addCategory(e){
		e.preventDefault();

		let token = localStorage.getItem('token')

		if(isActive){
			fetch('https://peaceful-cove-93097.herokuapp.com/api/users/addCategory', {
				method: 'POST',
				headers: {

					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`

				},
				body: JSON.stringify({
					id: id,
					name: categoryName,
					type: type
				})
			})
			.then(res => res.json())
			.then(data => {
				// console.log(data)

				if(data) {

					setCategoryName('');
					setType('');

					Swal.fire({
						icon: "success",
						title: "Category Added",
						text: "Successfully added a category."
					})

					Router.reload('/categories');

				} else {

					Swal.fire({
						icon: "error",
						title: "Unsuccessful Creation",
						text: "Category Creation Failed. Something went wrong."
					})
				}
			})
		}
	}

	return (
		<Fragment>
			<Head>
	            <title>Category | Thrifty</title>
	            <link rel="icon" href="/file.png" />
	        </Head>
			<h1 className="text-center my-4">Category</h1>
			<Container className="py-3">
				<Row>
					<Col xs={12} md={4} lg={4}>
						<Card className="my-3" style={{ width: '20rem' }}>
							<Card.Body>
							    <Card.Title>Add Category</Card.Title>
							    <Form onSubmit={e => addCategory(e)} autoComplete="off">
									<Form.Group controlId="categoryName">
										<Form.Label>Category Name</Form.Label>
										<Form.Control type="text" placeholder="Enter Category Name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required />
									</Form.Group>
									<Form.Group controlId="type">
										<Form.Label>Type</Form.Label>
											<Form.Control as="select" value={type} onChange={e => setType(e.target.value)}>
												<option value="" disabled>Select Type</option>
												<option>income</option>
												<option>expense</option>
										    </Form.Control>
									</Form.Group>
									<Button type='submit' variant="secondary" className="btn-block" disabled={!isActive}>Add Category
									</Button>
								</Form>
							</Card.Body>
						</Card>
					</Col>
					<Col xs={12} md={8} lg={8}>
						<Container>
							<h3>Category Overview</h3>
								<Table striped bordered hover responsive="md lg">
									<thead>
										<tr>
											<th>Name</th>
											<th>Type</th>
										</tr>
									</thead>
									<tbody>
										{categoryCards}
									</tbody>
								</Table>
						</Container>
					</Col>
				</Row>
			</Container>

		</Fragment>
	)
}