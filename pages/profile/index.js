import {Fragment, useState, useEffect, useContext} from 'react';
import Head from 'next/head';
import Image from 'next/image';

import {Jumbotron, Container, Card, Row, Col} from 'react-bootstrap';


//usercontext
import UserContext from '../../userContext';

export default function Profile(){

	const {user} = useContext(UserContext);

	//states for user details
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [balance, setBalance] = useState(0);
	// states to  get total expense and total income
	const [incomeBalance, setIncomeBalance] = useState(0);
	const [expenseBalance, setExpenseBalance] = useState(0);


	//get user details
	useEffect(() => {
		fetch('https://peaceful-cove-93097.herokuapp.com/api/users/details', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setName(`${data.firstName } ${data.lastName}`);
			setEmail(data.email);
			setBalance(data.balance);

			let income = data.records.filter(e => e.type === "income");
			let expense = data.records.filter(e => e.type === "expense")

			setIncomeBalance(income.map(e => parseInt(e.amount)).reduce((x,y) => x + y));
			setExpenseBalance(expense.map(e => parseInt(e.amount)).reduce((x,y) => x + y))
		})
	})

	
	return (
		<Fragment>
			<Head>
	            <title>Profile | Thrifty</title>
	            <link rel="icon" href="/userprofile.png" />
	        </Head>
	        <Container className="prof-container">
	        	<h1 className="text-center">Profile Page</h1>
	        	<Row>
		        	<Col xs={12} md={6} lg={6} className="col-prof">
		        		<Card className="mx-auto my-4 h-100" style={{ width: '23rem' }}>
		        			<Card.Body>
		        				<Card.Title className="text-center my-3"><strong>{name}</strong></Card.Title>
		        				<Card.Subtitle className="text-center mb-4 text-muted">Email: {email}</Card.Subtitle>
		        				<Card.Text className="text-center">Balance: &#8369;{balance}</Card.Text>
		        				<Card.Text className="text-center">Total Income: &#8369;{incomeBalance}</Card.Text>
		        				<Card.Text className="text-center">Total Expense: &#8369;{expenseBalance}</Card.Text>
		        			</Card.Body>
		        		</Card>
		        	</Col>
		        	
		        </Row>
		        <Row>
		        	<Container className="text-center pl-5">
		        		<Image className="profile_image mx-auto"
                            src="/data.svg"
                            alt="Picture for profile"
                            width={1235*0.5}
                            height={858*0.5}
                        />
		        	</Container>
		        </Row>
	        </Container>
		</Fragment>
	)
}