import {Fragment, useState, useEffect} from 'react';
import {Bar, Line} from 'react-chartjs-2';
import {Row, Col, Container, Form, Card, Button} from 'react-bootstrap'

import moment from 'moment';

import Head from 'next/head';

import 'chartjs-plugin-labels';

export default function Comparison(){

	//states to get months
	const [incomeMonths, setIncomeMonths] = useState([]);
	const [expenseMonths, setExpenseMonths] = useState([]);

	//states to get values of expense and income expense
	const [incomeBalance, setIncomeBalance] = useState([]);
	const [expenseBalance, setExpenseBalance] = useState([]);

	// states  to get income and expense records
	const [incomeRecords, setIncomeRecords] = useState([]);
	const [expenseRecords, setExpenseRecords] = useState([]);

	//states get all records
	const [allRecords, setAllRecords] = useState([]);

	//state to store balance overview
	const [allBalance, setAllBalance] = useState([]);

	//state to set start and end date 
	const [startDate, setStartDate] = useState("");
	const [endDate, setEndDate] = useState("");

	//state to check user date inputs
	const [isActive, setIsActive] = useState(false)

	//check date range details
	useEffect(() => {

		if(startDate !== '' && endDate !== ''){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	}, [startDate, endDate])

	// get all records of expense and income
	useEffect(() => {
		fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/records`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllRecords(data)
			setIncomeRecords(data.filter(record => record.type === "income"))
			setExpenseRecords(data.filter(record => record.type === "expense"))
		})

	}, [])

	//get months for income 
	useEffect(() => {
		let tempMonths = [];

		incomeRecords.forEach(element => {

			if(!tempMonths.find(month => month === moment(element.recordedOn).format('MMMM'))){

				tempMonths.push(moment(element.recordedOn).format('MMMM'));

			}

			// console.log(tempMonths) // Unsorted Array
			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

			tempMonths.sort((a,b) => {
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}

			})

			// console.log(tempMonths) // Sorted Array
			setIncomeMonths(tempMonths);
		})

	}, [incomeRecords])

	//get months for expense 
	useEffect(() => {
		let tempMonths = [];

		expenseRecords.forEach(element => {

			if(!tempMonths.find(month => month === moment(element.recordedOn).format('MMMM'))){

				tempMonths.push(moment(element.recordedOn).format('MMMM'));

			}

			// console.log(tempMonths) // Unsorted Array
			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

			tempMonths.sort((a,b) => {
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}

			})

			// console.log(tempMonths) // Sorted Array
			setExpenseMonths(tempMonths);
		})

	}, [expenseRecords])

	//get income balance
	useEffect(() => {

		setIncomeBalance(incomeMonths.map(month => {

			let nakuha = 0; 

			incomeRecords.forEach(element => {

				if(moment(element.recordedOn).format("MMMM") === month){

					nakuha += parseInt(element.amount)

				}

			})

			return nakuha
		}))

	}, [incomeMonths])


	//get expense balance
	useEffect(() => {

		setExpenseBalance(expenseMonths.map(month => {

			let gastos = 0; 

			expenseRecords.forEach(element => {

				if(moment(element.recordedOn).format("MMMM") === month){
					gastos += parseInt(element.amount)

				}

			})

			return gastos
		}))

	}, [expenseMonths])

	//get all records  of balances
	useEffect(() => {

		let newBalances = allRecords.map(balance => {
			return balance.balance
		})
		setAllBalance(newBalances);
		
	}, [allRecords])

	//data for bar charts
	//expense
	const expense = {

		labels: expenseMonths, // x-axis label
		datasets: [{

			label: 'Monthly Expense', //bar level
			backgroundColor: '#bdb2ff',
			borderColor: 'white',
			barThickness: 50,
			borderWidth: 1,
			hoverBackgroundColor: '#5e60ce',
			hoverBorderColor: 'black',
			data: expenseBalance // determinant for bars
		}]

	}

	//income 
	const income = {

		labels: incomeMonths, // x-axis label
		datasets: [{

			label: 'Monthly Income', //bar level
			backgroundColor: '#a0c4ff',
			borderColor: 'white',
			barThickness: 50,
			borderWidth: 1,
			hoverBackgroundColor: '#5390d9',
			hoverBorderColor: 'black',
			data: incomeBalance // determinant for bars
		}]

	}

	//options to be added to set
	const options = {
		scales: {

			yAxes: [
				{
					ticks: {
						beginAtZero: true,
						// max: 100
					}

				}
			]

		}, 
		responsive: true,
		plugins: {
		  	labels: {
		    	render: 'value'
		  	}
		}
	}

	//line data
	const data = {

		labels: allBalance, // x-axis label
		datasets: [{

			label: 'Budget Trend', //bar level
			backgroundColor: '#b5c99a',
			borderColor: 'white',
			barThickness: 50,
			borderWidth: 1,
			hoverBackgroundColor: '#97a97c',
			hoverBorderColor: 'black',
			data: allBalance // determinant for bars
		}]

	}

	function dateRange(e){
		e.preventDefault()

		// INPUT DATE FORMAT = "YYYY-MM-DD"

		let result = allRecords.filter(check => {
			if(moment(check.recordedOn).format("YYYY-MM-DD") === startDate || moment(check.recordedOn).format("YYYY-MM-DD") === endDate){
				return check
			}
		})

		setAllBalance(result.map(e => e.balance))
	}

	return(
		<Fragment>
			<Head>
	            <title>Budget Analysis | Thrifty</title>
	            <link rel="icon" href="/chart.png" />
	        </Head>
	        <h1 className="text-center my-4">Budget Analysis</h1>
	        <Container>
	        	<Row>
		        	<Col xs={12} md={6} lg={6}>
		        		<h2 className="text-center">Monthly Income</h2>
		        		<Bar data={income} options={options}/>
		        	</Col>
		        	<Col xs={12} md={6} lg={6}>
		        		<h2 className="text-center">Monthly Expense</h2>
		        		<Bar data={expense} options={options}/>
		        	</Col>
		        </Row>
		        <Row>
		        	<Col xs={12} md={4} lg={4}>
		        	<h3 className="text-center">Budget Trend</h3>
		        		<Container>
		        			<Card>
		        				<Card.Body>
		        					<Card.Title>Date Range</Card.Title>
		        					<Form onSubmit={e => dateRange(e)}>
		        						<Form.Group controlId="startDate">
		        							<Form.Label>Start Date</Form.Label>
		        							<Form.Control type="date" value={startDate} onChange={e => setStartDate(e.target.value)}/>
		        						</Form.Group>
		        						<Form.Group controlId="endtDate">
		        							<Form.Label>End Date</Form.Label>
		        							<Form.Control type="date" value={endDate} onChange={e => setEndDate(e.target.value)}/>
		        						</Form.Group>
		        						<Button className="btn-block" type="submit" variant="secondary" disabled={!isActive}>Submit</Button>
		        					</Form>
		        				</Card.Body>
		        			</Card>
		        		</Container>
		        	</Col>
		        	<Col xs={12} md={8} lg={8}>
		        		<Line data={data} options={options} />
		        	</Col>
		        </Row>
	        </Container>
		</Fragment>
	)
}