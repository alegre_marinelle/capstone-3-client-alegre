import {Fragment, useState, useEffect, useContext} from 'react';
import Head from 'next/head';

//react-bootstrap
import {Form, Button, Row, Col, Card, Container} from 'react-bootstrap';

//import sweet alert
import Swal from 'sweetalert2';

// To redirect the user use the Router component from nextJS
import Router from 'next/router';
import Link from 'next/link';

//user context
import UserContext from '../../userContext';

import {GoogleLogin} from 'react-google-login'

export default function Login(){

	const {user, setUser} = useContext(UserContext);
	// console.log(user)

	//create states for user input (state, setter function)
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//create state for conditionally rendering our button
	const [isActive, setIsActive] = useState(true);

	//check if form is filled
	useEffect(() => {

		if(email !== '' && password !== ''){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[email, password])

	function authenticate(e){

		e.preventDefault();

		fetch('https://peaceful-cove-93097.herokuapp.com/api/users/login', {

			method: "POST",
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.accessToken){

				localStorage.setItem('token', data.accessToken);

				fetch('https://peaceful-cove-93097.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json()) //response to json
				.then(data => {

					// console.log(data)

					localStorage.setItem('email', data.email);
					localStorage.setItem('userId', data._id);

					//after getting the user's details from API will set the global user state
					setUser({

						email: data.email,
						userId: data._id
					})

				})

				Swal.fire({
					icon: "success",
					title: "Successful Login",
					text: "You've been successfully logged in"
				})
				
				Router.push('/categories')

			} else {

				Swal.fire({
					icon: "error",
					title: "Unsuccessful Login",
					text: "User Aunthentication has failed."
				})
			}

			
		})

		//set the input states into their initial value
		setEmail("");
		setPassword("");

	}

	function authenticateGoogleToken(response){

		
		// console.log(response)
		fetch('https://peaceful-cove-93097.herokuapp.com/api/users/verify-google-id-token',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({
				tokenId: response.tokenId,
				accessToken: response.accessToken

			})

		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.accessToken !== 'undefined'){

				localStorage.setItem('token', data.accessToken)

				fetch('https://peaceful-cove-93097.herokuapp.com/api/users/details',{

					headers: {

							'Authorization': `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem('email', data.email)
					localStorage.setItem('userId', data._id)

					//after getting the user's details, update the global user state:
					setUser({

						email: data.email,
						userId: data._id


					})

					//Fire a sweetalert to inform the user of successful login:
					Swal.fire({

						icon: 'success',
						title: 'Successful Login'

					})

					Router.push('/categories')
				})

			} else {

				if(data.error === "google-auth-error"){
					Swal.fire({

						icon: 'error',
						title: 'Google Authentication Failed'

					})
				} else if(data.error === "login-type-error"){
					Swal.fire({

						icon: 'error',
						title: 'Login Failed.',
						text: 'You may have registered through a different procedure.'

					})
				}
			}
		})
	}

	return(
		<Fragment>
			<Head>
	            <title>Login | Thrifty</title>
	            // <link rel="icon" href="/userprofile.png" />
	        </Head>
			<Container>
				<Row>
					<Col xs={12} md={6} lg={6} className=" offset-md-3 mt-5">
						<Card className="my-3">
							<Card.Body>
								<Card.Title>Login</Card.Title>
								<Form onSubmit={e => authenticate(e)} autoComplete="off">
									<Form.Group controlId="userEmail">
										<Form.Label>Email:</Form.Label>
										<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
									</Form.Group>
									<Form.Group controlId="userPassword">
										<Form.Label>Password:</Form.Label>
										<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)}required/>
									</Form.Group>
									<Button variant="secondary" type="submit" className="btn-block" disabled={!isActive}>Submit</Button>
										
									<GoogleLogin 

										clientId="32051308750-o7hqa9mb21bqu3nrsvf2mp3d8q506s7h.apps.googleusercontent.com"
										buttonText="Login Using Google"
										onSuccess={authenticateGoogleToken}
										onFailure={authenticateGoogleToken}
										cookiePolicy={'single_host_origin'}
										className="w-100 text-center my-2 d-flex justify-content-center"

									/>
									<Link href="/register"><a role="button">Not yet registered? Sign Up </a></Link>
								</Form>
							</Card.Body>
						</Card>
					</Col>
				</Row>	
			</Container>
		</Fragment>
	)
}