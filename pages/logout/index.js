import {useContext, useEffect} from 'react';

import UserContext from '../../userContext';

import Router from 'next/router';


export default function Logout(){

	const {unsetUser} = useContext(UserContext)

	useEffect(() => {

		unsetUser()
		//redirect to the user login page
		Router.push('/login')

	}, []) //dependency array

	return null
}