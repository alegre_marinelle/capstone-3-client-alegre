import {Fragment, useState,useEffect, useContext} from 'react';
import Head from 'next/head';
import {Jumbotron, Form, Button, Row, Col, Card, Container} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../../userContext';

//import router from nextJS for user redirection
import Router from 'next/router';

//components
import Record from '../../components/Record'

export default function Search(){

	const {user} = useContext(UserContext);

	//states for searches
	const [categoryName, setCategoryName] = useState('');
	const [description, setDescription] = useState('');

	//State for conditionally rendering our button
	const [isActiveD, setIsActiveD] = useState(false);
	const [isActiveC, setIsActiveC] = useState(false);

	//state to store found data
	const [foundDesc, setFoundDesc] = useState([]);
	const [foundCat, setFoundCat] = useState([]);

	// get category input 
	useEffect(() => {

		if(categoryName !== ""){

			setIsActiveC(true)

		} else {

			setIsActiveC(false)

		}
	}, [categoryName])

	//get description input
	useEffect(() => {

		if(description !== ""){

			setIsActiveD(true)

		} else {

			setIsActiveD(false)

		}

	}, [description])

	/*
		a = "Hello World!"

		b = a.split(" "); ["hello", "world"]

		b = a.replaceAll(/[.,!?]/g,'').toLowerCase().split(' ')

	*/

	function searchDescription(e){
		e.preventDefault();

		let newDesc = description.replaceAll(/[.,!?]/g,'').toLowerCase().split(' ');

		if(newDesc.length > 1){

			fetch('https://peaceful-cove-93097.herokuapp.com/api/users/searchByDesc', {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					id: user.userId,
					description: description
				})
			})
			.then(res => res.json())
			.then(data => {

				let newArr = [];
				newArr.push(data);

				// console.log(data)
				if(newArr){

					setFoundDesc(newArr);
					setDescription('');

					// Router.reload('/search');

				} else {

					Swal.fire({
						icon: "error",
						title: "Search Failed",
						text: `${description} does not exists.`
					})

				}

				// console.log(foundDesc)
			})

		} else {

			fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/searchByDescription/${description}`, {
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				// console.log(data)
				if(data.length > 0){

					setFoundDesc(data);
					setDescription('');

				} else {

					Swal.fire({
						icon: "error",
						title: "Search Failed",
						text: `${description} does not exists.`
					})

				}
			})
		}
	}

	let desc = foundDesc.map(record => {
		return(
			<Record key={record._id} recordProp={record}/>
		)
	})



	function searchCategory(e){
		e.preventDefault();

		let newName = categoryName.replaceAll(/[.,!?]/g,'').toLowerCase().split(' ');

		if(newName.length > 1){

			fetch('https://peaceful-cove-93097.herokuapp.com/api/users/searchByCat', {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					id: user.userId,
					category: categoryName
				})
			})
			.then(res => res.json())
			.then(data => {
				let newArr = [];
				newArr.push(data);

				// console.log(data)
				if(newArr){
					
					setFoundCat(newArr);
					setDescription('');

					// Router.reload('/search');

				} else {

					Swal.fire({
						icon: "error",
						title: "Search Failed",
						text: `${description} does not exists.`
					})

				}
			})

		} else {

			fetch(`https://peaceful-cove-93097.herokuapp.com/api/users/searchByCategory/${categoryName}`, {
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				//console.log(data)
				if(data.length > 0){

					setFoundCat(data);
					setDescription('');

				} else {

					Swal.fire({
						icon: "error",
						title: "Search Failed",
						text: `${description} does not exists.`
					})

				}

			})
		}
	}

	let category = foundCat.map(record => {
		return(
			<Record key={record._id} recordProp={record}/>
		)
	})

	return (
		<Fragment>
			<Head>
	            <title>Search | Thrifty</title>
	            <link rel="icon" href="/search.png" />
	        </Head>
			<h1 className="text-center my-3 mt-4">Search Records</h1>
			<Container>
				<Row>
					<Col xs={12} md={4} lg={4}>
						<Card className="my-5" style={{ width: '20rem' }}>
							<Card.Body>
								<Card.Title>Search by Description</Card.Title>
								<Form onSubmit={e => searchDescription(e)} autoComplete="off">
									<Form.Group controlId="description">
										<Form.Label>Description</Form.Label>
										<Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} />
									</Form.Group>
									<Button className="btn-block" type='submit' variant="secondary" disabled={!isActiveD}>Search</Button>
								</Form>
							</Card.Body>
						</Card>
					</Col>
					<Col xs={12} md={8} lg={8}>
						<Container className="my-3">
							<h4>Search Result</h4>
							{desc}
						</Container>
					</Col>
				</Row>
				<Row>
					<Col xs={12} md={4} lg={4}>
						<Card className="my-5" style={{ width: '20rem' }}>
							<Card.Body>
								<Card.Title>Search by Category</Card.Title>
								<Form onSubmit={e => searchCategory(e)} autoComplete="off">
									<Form.Group controlId="categoryName">
										<Form.Label>Category</Form.Label>
										<Form.Control type="text" placeholder="Enter Category" value={categoryName} onChange={e => setCategoryName(e.target.value)} />
									</Form.Group>
									<Button className="btn-block" type='submit' variant="secondary" disabled={!isActiveC}>Search</Button>
								</Form>
							</Card.Body>
						</Card>
					</Col>
					<Col xs={12} md={8} lg={8}>
						<Container className="my-3">
							<h4>Search Result</h4>
							{category}
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}