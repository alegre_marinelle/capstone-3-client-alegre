import {Fragment, useContext} from 'react';
import Head from 'next/head';
// import styles from '../styles/Home.module.css'

import {Container, Row, Col, Card} from 'react-bootstrap';

//usercontext
import UserContext from '../userContext';

import Image from 'next/image';

export default function Home() {

    const {user} = useContext(UserContext);

    return (
        <Fragment>
             <Head>
                <title>Home | Thrifty</title>
                <link rel="icon" href="/wallet.png" />
            </Head>
            <Container className="text-center my-5">
                <Row>
                    <Col xs={12} md={6} lg={6} className="my-5">
                        <Container className="home">
                            Thrifty
                        </Container>
                    </Col>
                    <Col xs={12} md={6} lg={6} className="my-5">
                        <Image className="home_image mx-auto my-3"
                            src="/wallet.svg"
                            alt="Picture for home"
                            width={1235*0.8}
                            height={858*0.8}
                        />
                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
}

/*

<Container className="text-center">
    UNDER CONSTRUCTION
</Container>

*/
