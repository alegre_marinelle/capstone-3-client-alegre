import React from 'react';

//create a context from our React
const UserContext = React.createContext()

/*
	export the provier component of our react context which allows us to subscribe our components to changes made to our context. This means that the provider component will allow components inside it access to the valuse of our context.
*/
export const UserProvider = UserContext.Provider

export default UserContext